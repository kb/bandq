B & Q
*****

Django B&Q
https://www.kbsoftware.co.uk/docs/app-bandq.html

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-bandq
  # or
  virtualenv --python=python3 venv-bandq

  source venv-bandq/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
