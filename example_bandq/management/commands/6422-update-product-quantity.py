# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from bandq.service import BandQProduct, update_products


class Command(BaseCommand):

    help = "B&Q - update product (testing) #[ticket number]"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        products = [
            BandQProduct(
                sku="[SKU in here]",
                price=Decimal("[A decimal value in here perhaps?]"),
                quantity=0,
            ),
        ]
        update_products(products)
        self.stdout.write(f"{self.help} - Complete")
