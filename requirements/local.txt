-r base.txt
-e .
-e ../base
-e ../login
-e ../mail
black
django-debug-toolbar
factory-boy
GitPython
prettyprinter
pytest-cov
pytest-django
pytest-flakes
PyYAML
responses
rich
semantic-version
walkdir
