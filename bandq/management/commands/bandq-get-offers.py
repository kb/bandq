# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from bandq.service import get_offers


class Command(BaseCommand):

    help = "B&Q Offers - GET"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        offers = get_offers()
        pprint(offers, expand_all=True)
        count = len(set([x.offer_id for x in offers]))
        self.stdout.write(f"{self.help} - Complete. Found {count} products.")
