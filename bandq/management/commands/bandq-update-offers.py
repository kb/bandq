# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from bandq.service import update_offers, get_offers


class Command(BaseCommand):

    help = "B&Q Offers - UPDATE"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        offers = update_offers(get_offers)
        self.stdout.write(f"{self.help} - Complete. Updated {offers} products")
        