# -*- encoding: utf-8 -*-
class BandQError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        # return repr("%s, %s" % (self.__class__.__name__, self.value))
        return repr(f"{self.__class__.__name__}, {self.value}")
