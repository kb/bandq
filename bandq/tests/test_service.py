# -*- encoding: utf-8 -*-
import pytest
import responses

from decimal import Decimal
from http import HTTPStatus

# from rich.pretty import pprint

from bandq.models import BandQError
from bandq.service import (
    BandQProduct,
    get_offers,
    update_offers,
)


@pytest.mark.django_db
@responses.activate
def test_update_offers():
    responses.add(
        responses.POST,
        "https://marketplace.kingfisher.com/api/offers",
        json={
            "import_id": 999999
        },
        status=HTTPStatus.CREATED,
    )
    products = [
        BandQProduct(
            product_id="12345",
            offer_id=123456,
            price=12.34,
            shop_sku="S67890",
            quantity="1",
        )
    ]
    assert 1 == update_offers(products)


@pytest.mark.django_db
@responses.activate
def test_update_offers_exception():
    responses.add(
        responses.POST,
        "https://marketplace.kingfisher.com/api/offers",
        json={},
        status=HTTPStatus.INTERNAL_SERVER_ERROR,
    )
    products = [
        BandQProduct(
            product_id="4047424005774",
            offer_id=206393,
            price=7775.0,
            shop_sku="S11225",
            quantity="1",
        )
        # {
        #    "product_id": "4047424005774",
        #    "offer_id": 206393,
        #    "price": 7775.0,
        #    "shop_sku": "S11225",
        #    "quantity": "1",
        #    "update_delete": "update",
        #    "state_code": "11",
        # }
    ]
    with pytest.raises(BandQError) as e:
        update_offers(products)

    assert "BandQError, Cannot update products.  Status code 500" in str(
        e.value
    )


@pytest.mark.django_db
@responses.activate
def test_get_offers():
    responses.add(
        responses.GET,
        "https://marketplace.kingfisher.com/api/offers",
        json={
            "offers": [
                {
                    "product_references": [
                        {"reference": "4047424005774", "reference_type": "EAN"}
                    ],
                    "offer_id": 206393,
                    "price": 7775.0,
                    "shop_sku": "S11225",
                    "quantity": "5",
                    "update_delete": "update",
                    "state_code": "11",
                }
            ]
        },
        status=HTTPStatus.OK,
    )
    get_offers()


@pytest.mark.django_db
@responses.activate
def test_get_offers_exception():
    responses.add(
        responses.GET,
        "https://marketplace.kingfisher.com/api/offers",
        json={},
        status=HTTPStatus.INTERNAL_SERVER_ERROR,
    )
    # module_folder = pathlib.Path(__file__).resolve().parent
    # full_path = module_folder.joinpath("data", "test.json")
    # call_command("bandq-update-offers", [full_path])
    with pytest.raises(BandQError) as e:
        get_offers()

    assert (
        "Cannot retrieve offers.  Status code 500 Internal Server Error"
        in str(e.value)
    )
