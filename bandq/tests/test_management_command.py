# -*- encoding: utf-8 -*-
import pytest
import responses

from django.core.management import call_command
from http import HTTPStatus


@pytest.mark.django_db
@responses.activate
def test_bandq_get_offers():
    responses.add(
        responses.GET,
        "https://marketplace.kingfisher.com/api/offers",
        json={
            "offers": [
                {
                    "product_references": [
                        {"reference": "4047424005774", "reference_type": "EAN"}
                    ],
                    "offer_id": 206393,
                    "price": 7775.0,
                    "shop_sku": "S11225",
                    "quantity": "5",
                    "update_delete": "update",
                    "state_code": "11",
                }
            ]
        },
        status=HTTPStatus.OK,
    )
    # module_folder = pathlib.Path(__file__).resolve().parent
    # full_path = module_folder.joinpath("data", "test.json")
    # call_command("bandq-update-offers", [full_path])
    call_command("bandq-get-offers")
