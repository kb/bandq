# -*- encoding: utf-8 -*-
import attr
import json
import requests

from django.conf import settings
from http import HTTPStatus
from rich.pretty import pprint

from .models import BandQError


@attr.s
class BandQProduct:
    price = attr.ib()
    shop_sku = attr.ib()
    quantity = attr.ib()
    offer_id = attr.ib()
    product_id = attr.ib()


def _url_bandq_offers(offset, max):
    """GET - Offers: Browse."""
    return f"{settings.BANDQ_API_URL}offers?max={max}&offset={offset}"


def get_offers():
    result = []
    headers = {
        "Authorization": settings.BANDQ_SECRET_KEY,
        "Accept": "application/json",
    }
    max = 100
    offset = 0
    while True:
        url = _url_bandq_offers(offset, max)
        response = requests.request("GET", url, headers=headers, data={})
        if response.status_code == HTTPStatus.OK:
            data = response.json()
            products = data["offers"]
            for product in products:
                result.append(
                    BandQProduct(
                        price=product["price"],
                        shop_sku=product["shop_sku"],
                        quantity=product["quantity"],
                        offer_id=product["offer_id"],
                        product_id=product["product_references"][0][
                            "reference"
                        ],
                    )
                )
            if len(products) < max:
                break
            offset = offset + max
        else:
            raise BandQError(
                f"Cannot retrieve offers.  Status code {response.status_code} {response.reason}"
            )
    return result


def _update_offers(offer_updates):
    result = []
    for product in offer_updates:
        result.append(
            {
                'price': product.price,
                'product_id': product.product_id,
                'product_id_type': 'EAN',
                'quantity': product.quantity,
                'shop_sku': product.shop_sku,
                'state_code': '11', # '11' indicates a product in 'new' condition
                'update_delete': 'update' # This must be 'update'
            }
        )
    return result
    

def update_offers(offer_updates):

    # Required fields for updating offers:
        # price
        # shop_sku
        # quantity
        # product_id
        # product_id_type
        # state_code: state of the offer
        # update_delete: empty, update, or delete (empty = delete)

    url = f"{settings.BANDQ_API_URL}offers"
    headers = {
        "Authorization": settings.BANDQ_SECRET_KEY,
        "Content-Type": "application/json",
    }
    payload = json.dumps(
        {
            "offers": _update_offers(offer_updates),
        }
    )
    response = requests.request("POST", url, headers=headers, data=payload)
    if response.status_code == HTTPStatus.CREATED:
        pass
    else:
        raise BandQError(
            f"Cannot update products.  Status code {response.status_code}"
        )
    return len(offer_updates)
